;; A bare-bones .emacs configuration file. 

;; Set up the keyboard so the delete key on both the regular keyboard
;; and the keypad delete the character under the cursor and to the right
;; under X, instead of the default, backspace behavior.
(global-set-key [delete] 'delete-char)
(global-set-key [kp-delete] 'delete-char)
(global-set-key [home] 'beginning-of-buffer)

;; Inhibit the spash screen upon startup
(setq inhibit-splash-screen t)
(setq inhibit-startup-message t)

;; Always end a file with a newline
(setq require-final-newline t)

;; Stop at the end of the file, not just add lines
(setq next-line-add-newlines nil)

;; Set the fill column width - used in auto-filling
(setq fill-column 80)

;; An absolute must-have in my book 
(define-key ctl-x-map "\C-b" 'electric-buffer-list)

(define-key esc-map "g" 'goto-line)

(global-set-key "\M-t" 'beginning-of-buffer)
(global-set-key "\M-b" 'end-of-buffer)
(global-set-key "\M-i" 'overwrite-mode)

(defun scroll-one-line-ahead ()
  "Scroll ahead one line."
  (interactive)
  (scroll-up 1))

(defun scroll-one-line-behind ()
  "Scroll behind one line."
  (interactive)
  (scroll-down 1))

;; Useful for toggling between .h and .cpp files of a C++ class
(global-set-key "\M-o" 'ff-find-other-file)

(global-set-key [M-down] 'scroll-one-line-ahead)
(global-set-key [M-up]   'scroll-one-line-behind)


;; Add CMakeLists.txt names to the mode list.
(setq auto-mode-alist
      (append
       '(("CMakeLists\\.txt\\'" . cmake-mode))
       '(("\\.cmake\\'" . cmake-mode))
       auto-mode-alist))

(autoload 'cmake-mode "~/.emacs.d/cmake-mode.el" t)

;; Enable MOOS-IvP modes
(add-to-list 'load-path "~/Research/moos-ivp/trunk/editor-modes/")
(require 'moos-mode)

;; Add moos files to the mode list.
(setq auto-mode-alist
      (append
       '(("\\.moos\\'" . moos-mode))
              auto-mode-alist))
